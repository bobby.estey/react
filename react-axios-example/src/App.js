import PrimeNumbers from './components/PrimeNumbers.js';

function App() {
  return (
    <div className="App">
      <PrimeNumbers/>
    </div>
  )
}

export default App;
