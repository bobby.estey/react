import React from 'react';
import axios from 'axios';

export default class PrimeNumbers extends React.Component {

	constructor(props) {
		super(props);

		this.url = "http://localhost:6464/sieveOfEratosthenesControllerService/computeSieveOfEratosthenes?value=26";

		this.state = {
			primeNumbers: []
		}
	}

	componentDidMount() {
		axios.get(this.url)
			.then(res => {
				const primeNumbers = res.data;
				this.setState({ primeNumbers });
			})
	}

	render() {
		return (
			<ul>
				{
					this.state.primeNumbers
						.map(primeNumbers =>
							<li key={primeNumbers.id}>{primeNumbers.value}</li>
						)
				}
			</ul>
		)
	}
}