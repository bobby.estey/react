import Enzyme, { mount } from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import { render, screen } from '@testing-library/react';
import App from './App';

describe('<ApplicationContainer />', () => {
	Enzyme.configure({ adapter: new Adapter() });

	let component;
	// let shallowComponent;

	const config = {
		title: "title attribute",
		description: "description attribute"
	};

	const setUp = (props = {}) => {
		const component = mount(<App {...config} />);
		// const shallowComponent = shallow(<App {...config} />);
		return component;
	};

	beforeAll(() => {
		// console.log(
		//   "<<<<<AppTest - component.debug>>>>>\n\n" +
		//     component.debug() +
		//     "\n\n<<<<<AppTest - component.debug>>>>>"
		// );
	  });
	
	  beforeEach(() => {
		component = setUp();
	  });
	
	  afterEach(() => {
		// expect(component).toMatchSnapshot();
		component.unmount();
	  });
	
	  afterAll(() => {});

	test('screen.getByText', () => {
		render(<App />);
		const linkElement = screen.getByText(/react testing/i);
		expect(linkElement).toBeInTheDocument();
	});

	test("AppElementsStructure", () => {
		expect(component.find('div').getElements()).toHaveLength(2);
		expect(component.find('header').getElements()).toHaveLength(1);
		expect(component.find('p').getElements()).toHaveLength(2);
		expect(component.find('button').getElements()).toHaveLength(2);

		const buttonTwo = component.find('button[children="two"]');
		expect(buttonTwo).toHaveLength(1);
		buttonTwo.simulate('click');
	});

	test("AppPropertiesStructure", () => {
			expect(config).toHaveProperty("title");
			expect(config).toHaveProperty("description");
	});

	test("AppAttributesStructure", () => {
		expect(component.find("App").prop("title")).toEqual(
		  "title attribute"
		);
		expect(component.find("App").prop("description")).toEqual(
		  "description attribute"
		);
	  });

	  test("AppNodesStructure", () => {
		// expect(component.find("App").childAt(0).type()).toEqual(
		//   "div"
		// );
	
		// component.find("App").forEach((child) => {
		//   console.log("AppNodesStructure.forEach: " + "\n\n");
		// expect(child.type).toBeTruthy();
		// });
	  });

	  test("AppRender", () => {
		const component2 = render(component);
		// expect(component.title("abc")).toEqual("abc");
	
		// title: "title attribute",
		// description: "description attribute",
	  });
});
