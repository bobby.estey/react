import logo from './logo.svg';
import './App.css';

function App() {
	return (
		<div className="App">
			<header className="app-header">
				<img src={logo} className="App-logo" alt="logo" />
			</header>

			<div id="secondDiv">
				<p className="first-p">
					Rending <code>src/App.js</code>
				</p>
				<p id="second-p">
					Second Paragraph
				</p>

				React Testing with Jest and Enzyme

				<br /><br />

				<button id="1">one</button>&nbsp;&nbsp;
				<button id="2">two</button>
			</div>

			<br /><br />

			<footer>
				<b>Footer Section</b>
			</footer>
		</div>
	);
}

export default App;
