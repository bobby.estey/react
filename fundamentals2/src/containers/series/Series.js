import React, { Component } from "react";
import SeriesList from '../seriesList/SeriesList';

class Series extends Component {

  state = {
    series: [],
    seriesName: '',
    isFetching: false
  }

  // lifecycle hook - React instance is create, calls the componentDidMount function
  componentDidMount() {

    // const series = ["Vikings", "Turn"];

    // setTimeout(() => {
    //   this.setState({ series });
    // }, 2000);

    // fetch is going to this site and returning a Promise
    // fetch('http://api.tvmaze.com/search/shows?q=Vikings')
    // .then((response) => { console.log(response) })

    // fetch('http://api.tvmaze.com/search/shows?q=Vikings')
    // .then(response => response.json())
    // .then(json => console.log(json))

    // fetch('http://api.tvmaze.com/search/shows?q=Vikings')
    // .then(response => response.json())
    // .then(json => this.setState({ series: json }))
  }

  // onChange handler calls this function
  onSeriesInputChange = event => {

    this.setState({ seriesName: event.target.value, isFetching: true });

    fetch(`http://api.tvmaze.com/search/shows?q=${event.target.value}`)
    .then(response => response.json())
    .then(json => this.setState({ series: json, isFetching: false }))

    console.log(event);
    console.log(event.target.value);
  }

    // added Conditional Rendering
    render() {

        const { series, seriesName, isFetching } = this.state;

        return (
        <div>
            The length of series array - {this.state.series.length}
            <div>
                <input 
                    value={seriesName} 
                    type="text" 
                    onChange = {this.onSeriesInputChange} 
                />
            </div>

            { 
                !isFetching && series.length === 0 && seriesName.trim() === '' 
                &&
                <p>Enter series name into the input</p>
            }
            {
                !isFetching && series.length === 0 && seriesName.trim() !== '' 
                &&
                <p>No TV series has been found with this name</p>
            }
            { 
                isFetching && <p>Loading...</p>
            }
            {
                !isFetching && <SeriesList list={this.state.series} />
            }
            
        </div>
        )
    }
}

export default Series;
