import React from 'react';  // create React elements
import ReactDOM from 'react-dom'; // link between React and DOM
import './index.css';
import App from './components/App/App';
import reportWebVitals from './reportWebVitals';

// const getCurrentDate = () => {
//   const date = new Date();
//   return date.toDateString();
// }

// const greeting = <h1>Hello World Current Date: {getCurrentDate()}</h1>;

/*
ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

ReactDOM.render(App, document.getElementById('root')); // link to the App.js file - App function
*/

// render accepts upto 3 parameters, the React element to render, the DOM container and last is optional for a callback function
// root element is in public/index.html
ReactDOM.render(<App />, document.getElementById('root'));


// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
