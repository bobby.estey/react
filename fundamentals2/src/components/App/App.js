// https://www.youtube.com/watch?v=6Ied4aZxUzc
import React, { Component } from 'react';
import Intro from '../intro/Intro';
import Series from '../../containers/series/Series';
import './App.css';
import 'whatwg-fetch';

class App extends Component {

  state = {
    series: []
  }

  // lifecycle hook - React instance is create, calls the componentDidMount function
  componentDidMount() {

    // const series = ["Vikings", "Turn"];

    // setTimeout(() => {
    //   this.setState({ series });
    // }, 2000);

    // fetch is going to this site and returning a Promise
    // fetch('http://api.tvmaze.com/search/shows?q=Vikings')
    // .then((response) => { console.log(response) })

    // fetch('http://api.tvmaze.com/search/shows?q=Vikings')
    // .then(response => response.json())
    // .then(json => console.log(json))

    fetch('http://api.tvmaze.com/search/shows?q=Vikings')
    .then(response => response.json())
    .then(json => this.setState({ series: json }))
  }

  render() {
    return (
      <div>
        <Series/>
      </div>
    );
  }
}

export default App;
