// https://www.youtube.com/watch?v=10FNqoPpNbE
import React, { useState } from "react";
import Modal from "react-modal";

Modal.setAppElement("#root");

function ModalExample() {
  const [modalIsOpen, setModalIsOpen] = useState(false);
  return (
    <div className="modal-example">
      <button onClick={() => setModalIsOpen(true)}>Open modal</button>
      <Modal
        className="modal-example"
        isOpen={modalIsOpen}
        shouldCloseOnOverlayClick={false}
        onRequestClose={() => setModalIsOpen(false)}
      >
        <h2>Title</h2>
        <p>Description</p>
        <div>
          <button onClick={() => setModalIsOpen(false)}>OK</button>
        </div>
      </Modal>
    </div>
  );
}

export default ModalExample;
