import './App.css';
// import HookPassing from './hookPassing/HookPassing.tsx';
// import ModalExample from './modal/ModalExample.tsx';
// import PropsStates from './propsStates/PropsStates.tsx';
import PropertiesTutorial from './Components/PropertiesTutorial.tsx';
import StateHookTutorial from './Components/StateHookTutorial.tsx';

function App() {

// set up the object vehicle
  const vehicle = {
    year: "1967",
    make: "Plymouth",
    model: "Fury 3"
  };

  return (
    <div className="App">
      <h1>App.css</h1>
      
      {/* <PropertiesTutorial year="1969" make="Dodge" model="Daytona Charger" />
      <PropertiesTutorial year="1970" make="Plymouth" model="Superbird" /> */}
      <PropertiesTutorial {...vehicle} />

      <StateHookTutorial />
    </div>
  );
}

export default App;
