// https://www.codegrepper.com/code-examples/javascript/react+hooks+passing+array
import React from "react";
import ReactDOM from "react-dom";

const { useState, useCallback } = React;
function HookPassing() {
  const [theArray, setTheArray] = useState([]);
  const addEntryClick = () => {
    setTheArray([...theArray, `Entry ${theArray.length}`]);
  };
  return [
    <input type="button" onClick={addEntryClick} value="Add" />,
    <div>
      {theArray.map((entry) => (
        <div>{entry}</div>
      ))}
    </div>,
  ];
}

export default HookPassing;

//ReactDOM.render(<Example />, document.getElementById("root"));
