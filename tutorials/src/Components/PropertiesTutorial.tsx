// https://www.youtube.com/watch?v=eO0j1JN6Ttg
import React, { useState } from "react";

const PropertiesTutorial = (props: Vehicle) => {
  const [year, setYear] = useState(props.year);
  const [make, setMake] = useState(props.make);
  const [model, setModel] = useState(props.model);
  console.log(props.year, year, props.make, make, props.model, model);

  return (
    <div>
      <h1>Properties Tutorial</h1>
      <b>Year</b> &nbsp;&nbsp; {year} &nbsp;&nbsp; <b>Make</b>
      &nbsp;&nbsp; {make} &nbsp;&nbsp;
      <b>Model</b> &nbsp;&nbsp; {model}
    </div>
  );
};

interface Vehicle {
  year: string;
  make: string;
  model: string;
}

export default PropertiesTutorial;
