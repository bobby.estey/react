// https://www.youtube.com/watch?v=LlvBzyy-558
import React, { useState } from "react";

const StateHookTutorial = () => {
  // let counter = 0;
  // counter - variable
  // setCounter - function
  // useState initializes counter to 0
  const [counter, setCounter] = useState(0);

  const [name, setName] = useState("Bobby");

  const increment = () => {
    //counter++;
    setCounter(counter + 1);
    console.log(counter);
  };

  let updateName = (event) => {
    const newName = event.target.value;
    setName(newName);
  };

  return (
    <div>
      <h1>State Hook Tutorial</h1>
      <h2>Counter: {counter}</h2>
      <button onClick={increment}>Increment</button>&nbsp;&nbsp;
      <input placeholder="Enter Name " onChange={updateName} />
      &nbsp;&nbsp;
      {name}
    </div>
  );
};

export default StateHookTutorial;
